﻿using ExamWebAPI.DataModel;
using Microsoft.AspNetCore.Mvc;

namespace ExamWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HistoryController : Controller
    {
        private readonly InventoryContext dbContext;
        public HistoryController(InventoryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult GetHistory()
        {
            return Ok(dbContext.History.ToList());
        }
        //Create
        [HttpPost]
        public void AddLog(Log log)
        {
            dbContext.Add(log);
            dbContext.SaveChanges();
        }
        //Read
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetLog([FromRoute] int id)
        {
            Log log = dbContext.History.Find(id);
            if (log != null)
            {
                return Ok(log);
            }
            return BadRequest();
        }
        //Update
        [HttpPut]
        [Route("{id:int}")]
        public void UpdateLog([FromRoute] int id, Log updateLog)
        {
            Log toUpdateItem = dbContext.History.Find(id);
            if (toUpdateItem != null)
            {
                toUpdateItem.Surname=updateLog.Surname;
                toUpdateItem.ItemId = updateLog.ItemId;
                toUpdateItem.ExploitationYearBegin = updateLog.ExploitationYearBegin;
                toUpdateItem.ExploitationYearEnd = updateLog.ExploitationYearEnd;
                dbContext.SaveChanges();
            }
        }
        //Delete
        [HttpDelete]
        [Route("{id:int}")]
        public void DeleteLog([FromRoute] int id)
        {
            Log log = dbContext.History.Find(id);
            if (log != null)
            {
                dbContext.Remove(log);
                dbContext.SaveChanges();
            }
        }
    }
}

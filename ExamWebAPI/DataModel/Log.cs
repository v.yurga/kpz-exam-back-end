﻿using System.ComponentModel.DataAnnotations;

namespace ExamWebAPI.DataModel
{
    public class Log
    {
        [Key]
        public int Id { get; set; }
        public string Surname { get; set; }
        public int ItemId { get; set; }
        public int ExploitationYearBegin { get; set; }
        public int ExploitationYearEnd { get; set; }
    }
}

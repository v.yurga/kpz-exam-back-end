﻿using System.ComponentModel.DataAnnotations;

namespace ExamWebAPI.DataModel
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int YearProduction { get; set; }
        public int Room { get; set; }
        public int WriteOffYear { get; set; }
    }
}

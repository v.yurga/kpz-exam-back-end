﻿using Microsoft.EntityFrameworkCore;

namespace ExamWebAPI.DataModel
{
    public class InventoryContext: DbContext
    {
        public InventoryContext(DbContextOptions options) : base(options) { }
        public DbSet<Item> Items
        {
            get;
            set;
        }
        public DbSet<Log> History
        {
            get;
            set;
        }
    }
}

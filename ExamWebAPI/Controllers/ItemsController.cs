﻿using ExamWebAPI.DataModel;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.InteropServices;

namespace ExamWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItemsController : Controller
    {
        private readonly InventoryContext dbContext;
        public ItemsController(InventoryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult GetItems()
        {
            return Ok(dbContext.Items.ToList());
        }
        //Create
        [HttpPost]
        public void AddItem(Item item)
        {
            dbContext.Add(item);
            dbContext.SaveChanges();
        }
        //Read
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetItem([FromRoute] int id)
        {
            Item item = dbContext.Items.Find(id);
            if (item != null)
            {
                return Ok(item);
            }
            return BadRequest();
        }
        //Update
        [HttpPut]
        [Route("{id:int}")]
        public void UpdateItem([FromRoute] int id, Item updateItem)
        {
            Item toUpdateItem = dbContext.Items.Find(id);
            if (toUpdateItem != null)
            {
                toUpdateItem.Price = updateItem.Price;
                toUpdateItem.WriteOffYear=updateItem.WriteOffYear;
                toUpdateItem.Name=updateItem.Name;
                toUpdateItem.YearProduction = updateItem.YearProduction;
                toUpdateItem.Room=updateItem.Room;
                dbContext.SaveChanges();
            }
        }
        //Delete
        [HttpDelete]
        [Route("{id:int}")]
        public void DeleteItem([FromRoute] int id)
        {
            Item item = dbContext.Items.Find(id);
            if (item != null)
            {
                dbContext.Remove(item);
                dbContext.SaveChanges();
            }
        }
    }
}
